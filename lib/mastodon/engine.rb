require_relative '../../config/boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.

# require 'pkg-config'

require 'puma'
require 'rails'
require 'thor'

require 'hamlit-rails'
require 'pg'
require 'makara'
require 'pghero'
require 'dotenv-rails'

require 'aws-sdk-s3'
require 'fog/core'
require 'fog/openstack'
require 'paperclip'
require 'paperclip/av/transcoder'
require 'streamio-ffmpeg'
require 'blurhash'

require 'active_model_serializers'
require 'addressable'
require 'bootsnap'
require 'browser'
require 'charlock_holmes'
require 'iso-639'
require 'chewy'
require 'cld3'
require 'devise'
require 'devise-two-factor'

#require 'devise_pam_authenticatable2' #FIXME: optional?

require 'net-ldap'
require 'omniauth-cas'
require 'omniauth-saml'
require 'omniauth'

require 'discard'
require 'doorkeeper'
require 'fast_blank'
require 'fastimage'
require 'goldfinger'
require 'hiredis'
require 'redis-namespace'
require 'health_check'
require 'htmlentities'
require 'http'
require 'http_accept_language'
require 'http_parser.rb'
require 'httplog'
require 'idn'
require 'kaminari'
require 'link_header'
require 'mime-types'
require 'nilsimsa'
require 'nokogiri'
require 'nsa'
require 'oj'
require 'ostatus2'
require 'ox'
require 'parslet'
require 'parallel'
require 'posix-spawn'
require 'pundit'
require 'premailer/rails'
require 'rack/attack'
require 'rack/cors'
require 'rails-i18n'
require 'rails-settings-cached'
require 'redis'
require 'redis_lock'
require 'rqrcode'
require 'ruby-progressbar'
require 'sanitize'
require 'sidekiq'
require 'sidekiq-scheduler'
require 'sidekiq-unique-jobs'
require 'sidekiq-bulk'
require 'simple-navigation'
require 'simple_form'
require 'sprockets/railtie'
require 'stoplight'
require 'strong_migrations'
require 'tty-command'
require 'tty-prompt'
require 'twitter-text'
require 'tzinfo'
require 'webpacker'
require 'webpush'

require 'json/ld'
require 'rdf/normalize'

require 'private_address_check'

require 'annotate'

require 'lograge'
require 'redis-rails'

require 'concurrent-ruby'
require 'connection_pool'

# Dev dependencies

require 'fabrication'
require 'fuubar'
require 'i18n/tasks'
require 'pry-byebug'
require 'pry-rails'
require 'rspec-rails'

require 'capybara'
require 'climate_control'
require 'faker'
require 'microformats'
require 'rails-controller-testing'
require 'rspec-sidekiq'
require 'simplecov'
require 'webmock'
require 'parallel_tests'

require 'active_record_query_trace'
require 'annotate'
require 'better_errors'
require 'binding_of_caller'
require 'bullet'
require 'letter_opener'
require 'letter_opener_web'
require 'memory_profiler'
require 'rubocop'
require 'rubocop-rails'
require 'brakeman'
require 'bundler/audit'

require 'capistrano'
require 'capistrano-rails'
require 'capistrano-rbenv'
require 'capistrano-yarn'

require 'derailed_benchmarks'
require 'stackprof'

require_relative '../../app/lib/exceptions'
require_relative '../../lib/paperclip/lazy_thumbnail'
require_relative '../../lib/paperclip/gif_transcoder'
require_relative '../../lib/paperclip/video_transcoder'
require_relative '../../lib/paperclip/type_corrector'
require_relative '../../lib/mastodon/snowflake'
require_relative '../../lib/mastodon/version'
require_relative '../../lib/devise/two_factor_ldap_authenticatable'
require_relative '../../lib/devise/two_factor_pam_authenticatable'
require_relative '../../lib/chewy/strategy/custom_sidekiq'

Dotenv::Railtie.load

Bundler.require(:pam_authentication) if ENV['PAM_ENABLED'] == 'true'

require_relative '../../lib/mastodon/redis_config'

module Mastodon
  class Engine < ::Rails::Engine
    config.i18n.available_locales = [
      :ar,
      :ast,
      :bg,
      :bn,
      :br,
      :ca,
      :co,
      :cs,
      :cy,
      :da,
      :de,
      :el,
      :en,
      :eo,
      :'es-AR',
      :es,
      :et,
      :eu,
      :fa,
      :fi,
      :fr,
      :ga,
      :gl,
      :he,
      :hi,
      :hr,
      :hu,
      :hy,
      :id,
      :io,
      :it,
      :ja,
      :ka,
      :kk,
      :ko,
      :lt,
      :lv,
      :mk,
      :ms,
      :nl,
      :nn,
      :no,
      :oc,
      :pl,
      :'pt-BR',
      :'pt-PT',
      :ro,
      :ru,
      :sk,
      :sl,
      :sq,
      :'sr-Latn',
      :sr,
      :sv,
      :ta,
      :te,
      :th,
      :tr,
      :uk,
      :'zh-CN',
      :'zh-HK',
      :'zh-TW',
    ]

    config.i18n.default_locale = ENV['DEFAULT_LOCALE']&.to_sym

    unless config.i18n.available_locales.include?(config.i18n.default_locale)
      config.i18n.default_locale = :en
    end

    # config.paths.add File.join('app', 'api'), glob: File.join('**', '*.rb')
    # config.autoload_paths += Dir[Rails.root.join('app', 'api', '*')]

    config.active_job.queue_adapter = :sidekiq

    config.middleware.use Rack::Attack
    config.middleware.use Rack::Deflater

    config.to_prepare do
      Doorkeeper::AuthorizationsController.layout 'modal'
      Doorkeeper::AuthorizedApplicationsController.layout 'admin'
      Doorkeeper::Application.send :include, ApplicationExtension
      Devise::FailureApp.send :include, AbstractController::Callbacks
      Devise::FailureApp.send :include, HttpAcceptLanguage::EasyAccess
      Devise::FailureApp.send :include, Localized
    end

    initializer "webpacker.proxy" do |app|
      begin
        insert_middleware = Mastodon.webpacker.config.dev_server.present?
      rescue
        insert_middleware = nil
      end

      next unless insert_middleware

      app.middleware.insert_before(
        0, Webpacker::DevServerProxy, # "Webpacker::DevServerProxy" if Rails version < 5
        ssl_verify_none: true,
        webpacker: Mastodon.webpacker
      )
    end

    # Allow app overrides
    config.to_prepare do
      Dir.glob(Rails.root + "app/overrides/**/*_override*.rb").each do |c|
        require_dependency(c)
      end
    end
  end
end
