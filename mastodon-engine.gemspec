$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require_relative "lib/mastodon/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "mastodon-engine"
  spec.version     = Mastodon::Version
  spec.authors     = ["Alex Gleason"]
  spec.email       = ["alex@alexgleason.me"]
  spec.homepage    = "https://gitlab.com/soapbox-pub/mastodon-engine"
  spec.summary     = "Mastodon as a Ruby on Rails Engine."
  spec.description = "Fork of Mastodon turned into a Ruby on Rails Engine, so it can be used by other projects."
  spec.license     = "AGPL-3.0"

  # Alow pushing this gem to RubyGems.org.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "https://rubygems.org"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "LICENSE", "Rakefile", "README.md"]

  spec.add_dependency 'pkg-config', '~> 1.3'

  spec.add_dependency 'puma', '~> 4.2'
  spec.add_dependency 'rails', '~> 5.2.3'
  spec.add_dependency 'thor', '~> 0.20'

  spec.add_dependency 'hamlit-rails', '~> 0.2'
  spec.add_dependency 'pg', '~> 1.1'
  spec.add_dependency 'makara', '~> 0.4'
  spec.add_dependency 'pghero', '~> 2.3'
  spec.add_dependency 'dotenv-rails', '~> 2.7'

  spec.add_dependency 'aws-sdk-s3', '~> 1.48'
  spec.add_dependency 'fog-core', '<= 2.1.0'
  spec.add_dependency 'fog-openstack', '~> 0.3'
  spec.add_dependency 'paperclip', '~> 6.0'
  spec.add_dependency 'paperclip-av-transcoder', '~> 0.6'
  spec.add_dependency 'streamio-ffmpeg', '~> 3.0'
  spec.add_dependency 'blurhash', '~> 0.1'

  spec.add_dependency 'active_model_serializers', '~> 0.10'
  spec.add_dependency 'addressable', '~> 2.7'
  spec.add_dependency 'bootsnap', '~> 1.4'
  spec.add_dependency 'browser'
  spec.add_dependency 'charlock_holmes', '~> 0.7.6'
  spec.add_dependency 'iso-639'
  spec.add_dependency 'chewy', '~> 5.1'
  spec.add_dependency 'cld3', '~> 3.2.4'
  spec.add_dependency 'devise', '~> 4.7'
  spec.add_dependency 'devise-two-factor', '~> 3.1'

  # spec.add_dependency 'devise_pam_authenticatable2', '~> 9.2' #FIXME: optional?

  spec.add_dependency 'net-ldap', '~> 0.10'
  spec.add_dependency 'omniauth-cas', '~> 1.1'
  spec.add_dependency 'omniauth-saml', '~> 1.10'
  spec.add_dependency 'omniauth', '~> 1.9'

  spec.add_dependency 'discard', '~> 1.1'
  spec.add_dependency 'doorkeeper', '~> 5.2'
  spec.add_dependency 'fast_blank', '~> 1.0'
  spec.add_dependency 'fastimage'
  spec.add_dependency 'goldfinger', '~> 2.1'
  spec.add_dependency 'hiredis', '~> 0.6'
  spec.add_dependency 'redis-namespace', '~> 1.5'
  spec.add_dependency 'health_check' #FIXME: git: 'https://github.com/ianheggie/health_check', ref: '0b799ead604f900ed50685e9b2d469cd2befba5b'
  spec.add_dependency 'htmlentities', '~> 4.3'
  spec.add_dependency 'http', '~> 3.3'
  spec.add_dependency 'http_accept_language', '~> 2.1'
  spec.add_dependency 'http_parser.rb', '~> 0.6' #FIXME: git: 'https://github.com/tmm1/http_parser.rb', ref: '54b17ba8c7d8d20a16dfc65d1775241833219cf2', submodules: true
  spec.add_dependency 'httplog', '~> 1.3'
  spec.add_dependency 'idn-ruby'
  spec.add_dependency 'kaminari', '~> 1.1'
  spec.add_dependency 'link_header', '~> 0.0'
  spec.add_dependency 'mime-types', '~> 3.3'
  spec.add_dependency 'nilsimsa' #FIXME: git: 'https://github.com/witgo/nilsimsa', ref: 'fd184883048b922b176939f851338d0a4971a532'
  spec.add_dependency 'nokogiri', '~> 1.10'
  spec.add_dependency 'nsa', '~> 0.2'
  spec.add_dependency 'oj', '~> 3.9'
  spec.add_dependency 'ostatus2', '~> 2.0'
  spec.add_dependency 'ox', '~> 2.11'
  spec.add_dependency 'parslet'
  spec.add_dependency 'parallel', '~> 1.17'
  spec.add_dependency 'posix-spawn' #FIXME: git: 'https://github.com/rtomayko/posix-spawn', ref: '58465d2e213991f8afb13b984854a49fcdcc980c'
  spec.add_dependency 'pundit', '~> 2.1'
  spec.add_dependency 'premailer-rails'
  spec.add_dependency 'rack-attack', '~> 6.1'
  spec.add_dependency 'rack-cors', '~> 1.0'
  spec.add_dependency 'rails-i18n', '~> 5.1'
  spec.add_dependency 'rails-settings-cached', '~> 0.6'
  spec.add_dependency 'redis', '~> 4.1'
  spec.add_dependency 'mario-redis-lock', '~> 1.2'
  spec.add_dependency 'rqrcode', '~> 0.10'
  spec.add_dependency 'ruby-progressbar', '~> 1.10'
  spec.add_dependency 'sanitize', '~> 5.1'
  spec.add_dependency 'sidekiq', '~> 5.2'
  spec.add_dependency 'sidekiq-scheduler', '~> 3.0'
  spec.add_dependency 'sidekiq-unique-jobs', '~> 6.0'
  spec.add_dependency 'sidekiq-bulk', '~>0.2.0'
  spec.add_dependency 'simple-navigation', '~> 4.1'
  spec.add_dependency 'simple_form', '~> 4.1'
  spec.add_dependency 'sprockets-rails', '~> 3.2'
  spec.add_dependency 'stoplight', '~> 2.1.3'
  spec.add_dependency 'strong_migrations', '~> 0.4'
  spec.add_dependency 'tty-command', '~> 0.9'
  spec.add_dependency 'tty-prompt', '~> 0.19'
  spec.add_dependency 'twitter-text', '~> 1.14'
  spec.add_dependency 'tzinfo-data', '~> 1.2019'
  spec.add_dependency 'webpacker', '~> 4.0'
  spec.add_dependency 'webpush'

  spec.add_dependency 'json-ld' #FIXME: git: 'https://github.com/ruby-rdf/json-ld.git', ref: 'e742697a0906e74e8bb777ef98137bc3955d981d'
  spec.add_dependency 'json-ld-preloaded', '~> 3.0'
  spec.add_dependency 'rdf-normalize', '~> 0.3'

  spec.add_dependency 'private_address_check', '~> 0.5'

  spec.add_dependency 'annotate', '~> 2.7'

  spec.add_dependency 'lograge', '~> 0.11'
  spec.add_dependency 'redis-rails', '~> 5.0'

  spec.add_dependency 'concurrent-ruby'
  spec.add_dependency 'connection_pool'

  spec.add_dependency 'fabrication', '~> 2.20'
  spec.add_dependency 'fuubar', '~> 2.4'
  spec.add_dependency 'i18n-tasks', '~> 0.9'
  spec.add_dependency 'pry-byebug', '~> 3.7'
  spec.add_dependency 'pry-rails', '~> 0.3'
  spec.add_dependency 'rspec-rails', '~> 3.8'

  spec.add_dependency 'capybara', '~> 3.29'
  spec.add_dependency 'climate_control', '~> 0.2'
  spec.add_dependency 'faker', '~> 2.5'
  spec.add_dependency 'microformats', '~> 4.1'
  spec.add_dependency 'rails-controller-testing', '~> 1.0'
  spec.add_dependency 'rspec-sidekiq', '~> 3.0'
  spec.add_dependency 'simplecov', '~> 0.17'
  spec.add_dependency 'webmock', '~> 3.7'
  spec.add_dependency 'parallel_tests', '~> 2.29'

  spec.add_dependency 'active_record_query_trace', '~> 1.6'
  spec.add_dependency 'better_errors', '~> 2.5'
  spec.add_dependency 'binding_of_caller', '~> 0.7'
  spec.add_dependency 'bullet', '~> 6.0'
  spec.add_dependency 'letter_opener', '~> 1.7'
  spec.add_dependency 'letter_opener_web', '~> 1.3'
  spec.add_dependency 'memory_profiler'
  spec.add_dependency 'rubocop', '~> 0.74'
  spec.add_dependency 'rubocop-rails', '~> 2.3'
  spec.add_dependency 'brakeman', '~> 4.6'
  spec.add_dependency 'bundler-audit', '~> 0.6'

  spec.add_dependency 'capistrano', '~> 3.11'
  spec.add_dependency 'capistrano-rails', '~> 1.4'
  spec.add_dependency 'capistrano-rbenv', '~> 2.1'
  spec.add_dependency 'capistrano-yarn', '~> 2.0'

  spec.add_dependency 'derailed_benchmarks'
  spec.add_dependency 'stackprof'

  spec.add_dependency 'listen', '~> 3.2.1'
end
